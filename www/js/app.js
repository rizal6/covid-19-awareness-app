// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ngCordova'])

.factory('myHelper', function ($cordovaToast) {
    function showToast($message, $duration = "long") {      
        $cordovaToast
            .show($message, $duration, 'bottom')
            .then(function (success) {
                // success
            }, function (error) {
                // error
            });
    
        console.log("Toast:" . $message);
        
        return true;
    }

    return {
        showToast: showToast
    }
})

.run(function ($ionicPlatform, $ionicHistory, $ionicPopup, $state, $http, $rootScope) {
    $ionicPlatform.ready(function () { 
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            windoq.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            window.cordova.plugins.Keyboard.disableScroll(true);
            window.cordova.plugins.Keyboard.hideFormAccessoryBar(false);                 
        }
        Keyboard.hideFormAccessoryBar(false);
        Keyboard.hideKeyboardAccessoryBar(false);
        
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        if(window.MobileAccessibility){
           window.MobileAccessibility.usePreferredTextZoom(false);
        }
    });

    $ionicPlatform.registerBackButtonAction(function(event) {
        if($state.current.name == "app.home") {
            $ionicPopup.confirm({
                title: 'Konfirmasi',
                template: 'Apakah anda yakin ingin keluar dari aplikasi?'
            }).then(function(res) {
                if (res) {
                    ionic.Platform.exitApp();
                }
            })
        } else {
            $ionicHistory.goBack();
        }
    }, 100);
})

.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'templates/menu.html',
            controller: 'AppCtrl'
        })

        .state('app.registrasi', {
            url: '/registrasi',
            views: {
              'menuContent': {
                templateUrl: 'templates/registrasi.html',
                controller: 'RegisterCtrl'
              }
            }
          })

          .state('app.peta', {
              url: '/peta',
              views: {
                'menuContent': {
                  templateUrl: 'templates/peta.html',
                  controller: 'PetaCtrl'
                }
              }
            })
          .state('app.update', {
            url: '/update',
            views: {
              'menuContent': {
                templateUrl: 'templates/update.html',
                controller : 'UpdateCtrl'
              }
            }
          })
          .state('app.sehat', {
            url: '/sehat',
            views: {
              'menuContent': {
                templateUrl: 'templates/sehat.html',
                controller: 'CloseCtrl'
              }
            }
          })
          .state('app.gejala', {
            url: '/gejala',
            views: {
              'menuContent': {
                templateUrl: 'templates/gejala.html'
              }
            }
          })
          .state('app.sick', {
            url: '/sick',
            views: {
              'menuContent': {
                templateUrl: 'templates/sick.html',
                controller: 'CloseCtrl'
              }
            }
          })
          .state('app.about', {
            url: '/about',
            views: {
              'menuContent': {
                templateUrl: 'templates/about.html'
              }
            }
          })
          .state('app.beranda', {
            url: '/beranda',
            views: {
              'menuContent': {
                templateUrl: 'templates/beranda.html',
                controller: 'BerandaCtrl'
              }
            }
          })

        .state('app.map_address', {
            url: '/map_address',
            views: {
                'menuContent': {
                    templateUrl: 'templates/map_address.html',
                    controller: 'MapAddressCtrl'
                }
            }
        });


    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/registrasi');
});
