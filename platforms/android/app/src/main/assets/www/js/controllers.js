angular.module('starter.controllers', [])

.controller('AppCtrl', function ($rootScope, $ionicHistory, $state, $http, myHelper, $q, $cordovaGeolocation, myHelper, $ionicLoading) {
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    $rootScope.$on('$ionicView.enter', function(e) {
        $rootScope.getLocation();
    });

    // Basic configuration
    // $rootScope.serverURL        = "http://localhost/covid-19-awareness-web/api.php";
    $rootScope.serverURL        = "https://covid19.super-kiosk.com/api.php";
    $rootScope.appVersion       = "0.0.1";
    $rootScope.appVersionCode   = 1;
    $rootScope.appVersionCodes  = 1; //version from config.xml
    $rootScope.appType          = "user";

    $rootScope.log = function($message) {
        if ($rootScope.logEnabled) {
            console.log($message);
        }
    }

    $rootScope.getLocation = function() {
        var options = {timeout: 10000, enableHighAccuracy: true};
        $cordovaGeolocation.getCurrentPosition(options).then(function(position){
            $rootScope.latLng  = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            localStorage.setItem("originLat", position.coords.latitude);
            localStorage.setItem("originLng", position.coords.longitude);
        }, function(error){
            $rootScope.log("Could not get location");
            // myHelper.showToast("Could not get location. You need accept permission");
            $ionicLoading.hide();
        });
    }
})

.controller('PetaCtrl', function($state, $http, $rootScope, $scope, $cordovaGeolocation, $ionicLoading, $ionicHistory, myHelper){
    
    $scope.$on("$ionicView.beforeEnter", function(event, data){

        var lastUpdate = localStorage.getItem("userLastUpdate");
        console.log(lastUpdate);
        if (lastUpdate){
            var d = new Date();
            var months = d.getMonth()+1;
            if (d.getDate()<10){
                var daters = "0"+d.getDate();
            }else {
                var daters = d.getDate();
            }
            if (months<10){
                var month = "0"+months;
            }else {
                var month = months;
            }
            var timestamps = d.getFullYear()+"-"+month+"-"+daters;
            console.log(timestamps);
            if (timestamps == lastUpdate){
                var options    = {timeout: 10000, enableHighAccuracy: true};

                $scope.distanceLatitude = function (lat1, lon1, lat2, lon2, unit){
                    var radlat1 = Math.PI * lat1/180
                    var radlat2 = Math.PI * lat2/180
                    var theta = lon1-lon2
                    var radtheta = Math.PI * theta/180
                    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                    if (dist > 1) {
                        dist = 1;
                    }
                    dist = Math.acos(dist)
                    dist = dist * 180/Math.PI
                    dist = dist * 60 * 1.1515
                    return dist.toFixed(2).split(".");
                }

                var latLng = new google.maps.LatLng(localStorage.getItem("originLat"), localStorage.getItem("originLng"));

                var mapOptions = {
                    center: latLng,
                    zoom: 12,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    maxZoom: 20,
                    mapTypeControl: false,
                    disableDefaultUI: true
                };

                $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: $scope.map,
                    title: 'Posisi Kiosk Anda',
                    icon: {
                        url: "img/icon-myposition.svg", // url
                        scaledSize: new google.maps.Size(25, 30), // scaled size
                        origin: new google.maps.Point(0,0), // origin
                        anchor: new google.maps.Point(0, 0) 
                    }
                });
                $scope.map.panTo(latLng);

                $http({
                    method : "POST",
                    url : $rootScope.serverURL,
                    data : {
                        appType          : $rootScope.appType,
                        appVersion       : $rootScope.appVersion,
                        appVersionCode   : $rootScope.appVersionCode,
                        act              : "GetStatusUserAll"
                    }
                }).then(function mySucces(response) {
                    if (response.data.request.status == "success") {
                        for (var i=0; i<=response.data.row.length-1; i++){
                            if (response.data.row[i].last_latitude != null){
                                var distanceValue = $scope.distanceLatitude(response.data.row[i].last_latitude, 
                                                                        response.data.row[i].last_longitude, 
                                localStorage.getItem("originLat"), localStorage.getItem("originLng"), 'K');
                                if (distanceValue[0] <= 5){
                                    var latMember = new google.maps.LatLng(response.data.row[i].last_latitude, response.data.row[i].last_longitude);

                                    var imgStatus = "img/icon-posisiSehat.svg";
                                    if (response.data.row[i].last_status == "suspect"){
                                        imgStatus = "img/icon-posisiPositif.svg";
                                    } else  if (response.data.row[i].last_status == "sick"){
                                        imgStatus = "img/icon-posisiNegatif.svg";
                                    }
                                    // console.log(imgStatus);
                                    var marker = new google.maps.Marker({
                                        position: latMember,
                                        map: $scope.map,
                                        title: response.data.row[i].full_name,
                                        icon: {
                                            url: imgStatus, //"img/icon-marker-customer.png", // url
                                            scaledSize: new google.maps.Size(25, 30), // scaled size
                                            origin: new google.maps.Point(0,0), // origin
                                            anchor: new google.maps.Point(0, 0) 
                                        }
                                    });
                                    $scope.map.panTo(latMember);
                                }
                            }
                        }
                    }
                });

                var circle = new google.maps.Circle({
                    map: $scope.map,
                    center: latLng,
                    radius: 5000,  //metres   
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 0,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35
                });
                circle.bindTo('center', marker, 'position');
            } else {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go("app.update");
            }
        } else {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go("app.update");
            
        }

    })
})

.controller('MapAddressCtrl', function($rootScope, $scope, $cordovaGeolocation, $ionicLoading, $ionicHistory, myHelper){
    var mapOptions = {
        zoom             : 16,
        mapTypeId        : google.maps.MapTypeId.ROADMAP,
        maxZoom          : 20,
        mapTypeControl   : false,
        disableDefaultUI : true
    };

    $scope.map       = new google.maps.Map(document.getElementById("map"), mapOptions);
    $scope.geocoder  = new google.maps.Geocoder(); 
    $scope.latLng    = new google.maps.LatLng(localStorage.getItem("originLat"), localStorage.getItem("originLng"));

    $scope.dataLocation = "";

    var input     = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    var markers   = [];

    var button = document.getElementById("buttonSet");
    $scope.map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(button);

    google.maps.event.addListener($scope.map, "dragend", function() {
        $rootScope.log("Map draging stopped");

        $scope.latLng = new google.maps.LatLng($scope.map.getCenter().lat(), $scope.map.getCenter().lng());

        $scope.geocoder.geocode({
            'latLng': $scope.latLng
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $rootScope.log("Geocoding from Map draging successful: " + results[0].formatted_address);
                $scope.dataLocation = "";
                $scope.$apply(function () {
                    $rootScope.log("Updating pac input to dragged location");
                    $("#pac-input").val(results[0].formatted_address);
                    $scope.dataLocation = results[0].formatted_address;
                });
            }
        });
    });    

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                $rootScope.log("Returned place contains no geometry");
                return;
            }
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        $scope.map.fitBounds(bounds);
    });

    $scope.$on("$ionicView.afterEnter", function(event, data) {
        var options = {timeout: 10000, enableHighAccuracy: true};

        if (localStorage.getItem("originLat") && localStorage.getItem("originLng")) {
            $rootScope.log("Panning to last known Lat Long");
            $scope.latLng = new google.maps.LatLng(localStorage.getItem("originLat"), localStorage.getItem("originLng"));
            $scope.map.panTo($scope.latLng);

            $scope.geocoder.geocode({
                'latLng': $scope.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    $rootScope.log("Geocoding from last known lat long panning successful: " + results[0].formatted_address);
                    $scope.dataLocation = "";
                    $scope.$apply(function () {
                        var address = $("#pac-input").val();
                        $("#pac-input").val(results[0].formatted_address);
                        $scope.dataLocation = results[0].formatted_address;
                    });
                }
            });
        } else {
            $rootScope.log("Panning to current location");
            $cordovaGeolocation.getCurrentPosition(options).then(function(position){

                $scope.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                $scope.map.panTo($scope.latLng);

                $scope.geocoder.geocode({
                    'latLng': $scope.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $rootScope.log("Geocoding from current location panning successful: " + results[0].formatted_address);
                        $scope.dataLocation = "";
                        $scope.$apply(function () {
                            var address = $("#pac-input").val();
                            $("#pac-input").val(results[0].formatted_address);
                            $scope.dataLocation = results[0].formatted_address;               
                        });
                    }
                });
            }, function(error){
                $rootScope.log("Could not get location");
                myHelper.showToast("Could not get location. You need accept permission");
                $ionicLoading.hide();
            });
        }
    });

    $scope.setLocationOrigin = function() {
        localStorage.setItem("changeAddress", "true");
        localStorage.setItem("LocationOriginAddress", $("#pac-input").val());
        localStorage.setItem("originLat", $scope.map.getCenter().lat());
        localStorage.setItem("originLng", $scope.map.getCenter().lng());

        $ionicHistory.goBack();
    }

    // Workaround to enable touch selecting the autocomplete addresses
    $scope.disabledTap = function() {
        var container = document.getElementsByClassName('pac-container');
        angular.element(container).attr('data-tap-disabled', 'true');
    };

    $scope.getNavigate = function(){
        var options = {timeout: 5000,maximumAge: 3000, enableHighAccuracy: true};
        $cordovaGeolocation.getCurrentPosition(options).then(function(position){

                $scope.latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                $scope.map.panTo($scope.latLng);

                $scope.geocoder.geocode({
                    'latLng': $scope.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $rootScope.log("Geocoding from current location panning successful: " + results[0].formatted_address);
                        $scope.dataLocation = "";
                        $scope.$apply(function () {
                            var address = $("#pac-input").val();
                            $("#pac-input").val(results[0].formatted_address);
                            $scope.dataLocation = results[0].formatted_address;               
                        });
                    }
                });


        }, function(error){
            $rootScope.log("Could not get location");
            myHelper.showToast("Could not get location. You need accept permission");
            $ionicLoading.hide();
        });
    }

    $scope.removeAddress = function(){
        $scope.dataLocation = "";
    }
})

.controller('CloseCtrl', function($ionicHistory, $state, $scope, $stateParams) {

    $scope.onClose = function(){
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        $state.go("app.beranda");
    }

})

.controller('UpdateCtrl', function($state, $rootScope, $scope, $stateParams, myHelper, $http, $ionicHistory, $ionicPopup) {

    $scope.formData = {};
    $scope.formData.checkedSneeze = false;
    $scope.formData.checkedCough = false;
    $scope.formData.checkedTired = false;
    $scope.formData.checkedFever = false;
    $scope.formData.checkedBreath = false;

    $scope.$on('$ionicView.afterEnter', function() {
        if (localStorage.getItem("userId")){
             $scope.btnNext = false;
            if (localStorage.getItem("LocationOriginAddress")) {
                $scope.formData.address = localStorage.getItem("LocationOriginAddress");
                $scope.formData.address_lat = localStorage.getItem("originLat"); //;
                $scope.formData.address_long = localStorage.getItem("originLng");

                $scope.hideAddress = false;

                localStorage.removeItem("LocationOriginAddress");
                localStorage.removeItem("originLat");
                localStorage.removeItem("originLng");
            }
            $rootScope.getLocation();

        } else {
           $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go("app.registrasi");
        }
    }); 

    $scope.loadData = function(){
        $http({
            method : "POST",
            url  : $rootScope.serverURL,
            data : {
                appType          : $rootScope.appType,
                appVersion       : $rootScope.appVersion,
                appVersionCode   : $rootScope.appVersionCode,
                act              : "GetStatusUser",
                user_id          : localStorage.getItem("userId")
            }
        }).then(function mySucces(response) {
            if (response.data.request.status == "success") {
                var userStatus = "normal";
                var lastUpdate = "";
                if (response.data.request.status_update){
                    userStatus = response.data.request.status_update.status;
                    lastUpdate = response.data.request.status_update.update_date;
                } 

                lastUpdate = lastUpdate.split(" ");
                localStorage.setItem("userLastUpdate", lastUpdate[0]);
                localStorage.setItem("userLastStatus", userStatus);
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: "Status User Tidak Ditemukan",
                    template: response.data.request.message
                });
            }
        }, function myError(response) {
            myHelper.showToast("Koneksi gagal, coba kembali nanti");
            $scope.btnNext = false;
        });
    }

    $scope.doUpdate = function(){
      $http({
            method : "POST",
            url  : $rootScope.serverURL,
            data : {
                appType          : $rootScope.appType,
                appVersion       : $rootScope.appVersion,
                appVersionCode   : $rootScope.appVersionCode,
                act              : "PutStatusUser",
                checkedCough     : $scope.formData.checkedCough,
                checkedSneeze    : $scope.formData.checkedSneeze,
                checkedTired     : $scope.formData.checkedTired,
                checkedFever     : $scope.formData.checkedFever,
                checkedBreath    : $scope.formData.checkedBreath,
                userId           : localStorage.getItem("userId"),
                userPhone        : localStorage.getItem("userPhone"),
                address_lat      : localStorage.getItem("originLat"), //$scope.formData.address_lat,
                address_long     : localStorage.getItem("originLng")
            }
        }).then(function mySucces(response) {
            if (response.data.request.status == "success") {
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });

                $scope.loadData();

                if (response.data.request.update_status == "sick"){
                    $state.go("app.sick");
                } else if (response.data.request.update_status == "suspect"){
                    $state.go("app.gejala");
                } else {
                    $state.go("app.sehat");
                }

                $scope.formData.checkedSneeze = false;
                $scope.formData.checkedCough = false;
                $scope.formData.checkedTired = false;
                $scope.formData.checkedFever = false;
                $scope.formData.checkedBreath = false;
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: "Update Kesehatan Anda Gagal",
                    template: response.data.request.message
                });
                $scope.btnNext = false;
            }
        }, function myError(response) {
            myHelper.showToast("Koneksi gagal, coba kembali nanti");
            $scope.btnNext = false;
        });
    }
})

.controller('RegisterCtrl', function($state, $rootScope, $scope, $stateParams, myHelper, $http, $ionicHistory, $ionicPopup) {

    $scope.formData = {};
    $scope.formData.fullName = "";
    $scope.formData.phoneNo  = "";
    $scope.formData.age      = "";
    $scope.formData.address  = "";
    $scope.formData.gender   = "";
    $scope.formData.address_lat = localStorage.getItem("originLat");
    $scope.formData.address_long = localStorage.getItem("originLng");;

    $errorMessage = "";

    $scope.hideAddress = true;
    localStorage.removeItem("LocationOriginAddress");

    $scope.$on('$ionicView.afterEnter', function() {
        if (localStorage.getItem("userId")){
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go("app.beranda");
        } else {
            $scope.btnNext = false;
            // if (localStorage.getItem("LocationOriginAddress")) {
            //     $scope.formData.address = localStorage.getItem("LocationOriginAddress");
            //     $scope.formData.address_lat = localStorage.getItem("originLat"); //;
            //     $scope.formData.address_long = localStorage.getItem("originLng");

            //     $scope.hideAddress = false;

            //     localStorage.removeItem("LocationOriginAddress");
            //     localStorage.removeItem("originLat");
            //     localStorage.removeItem("originLng");
            // }
            $rootScope.getLocation();
        }
    }); 

    $scope.doRegister = function(){
        $scope.btnNext = true;
        $http({
            method : "POST",
            url  : $rootScope.serverURL,
            data : {
                appType          : $rootScope.appType,
                appVersion       : $rootScope.appVersion,
                appVersionCode   : $rootScope.appVersionCode,
                act              : "RegisterMember",
                full_name        : $scope.formData.fullName,
                phone_no         : $scope.formData.phoneNo,
                address          : $scope.formData.address,
                gender           : $scope.formData.gender,
                age              : $scope.formData.age,
                address_lat      : localStorage.getItem("originLat"), //$scope.formData.address_lat,
                address_long     : localStorage.getItem("originLng")
            }
        }).then(function mySucces(response) {
            if (response.data.request.status == "success") {

                localStorage.setItem("userId", response.data.row.id);
                localStorage.setItem("userFullname", response.data.row.full_name);
                localStorage.setItem("userPhone", response.data.row.phone);
                localStorage.setItem("userAddress", response.data.row.location);
                localStorage.setItem("userAddressLat", response.data.row.last_lat);
                localStorage.setItem("userAddressLong", response.data.row.last_long);
                var lastUpdate = response.data.row.last_update.split(" ");
                localStorage.setItem("userLastUpdate", lastUpdate[0]);

                $ionicHistory.nextViewOptions({
                    disableBack: true
                });
                $state.go("app.beranda");

                myHelper.showToast(response.data.request.message);
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: "Registrasi Gagal",
                    template: response.data.request.message
                });
                $scope.btnNext = false;
            }
        }, function myError(response) {
            myHelper.showToast("Koneksi gagal, coba kembali nanti");
            $scope.btnNext = false;
        });
    }
})

.controller('BerandaCtrl', function($rootScope, $scope, $stateParams, $ionicHistory, $http, $ionicPopup, myHelper, $state) {

    $scope.loadData = function(){
        $http({
            method : "POST",
            url  : $rootScope.serverURL,
            data : {
                appType          : $rootScope.appType,
                appVersion       : $rootScope.appVersion,
                appVersionCode   : $rootScope.appVersionCode,
                act              : "GetStatusUser",
                user_id          : localStorage.getItem("userId")
            }
        }).then(function mySucces(response) {
            if (response.data.request.status == "success") {
                var userStatus = "normal";
                if (response.data.request.status_update){
                    userStatus = response.data.request.status_update.status;
                    $scope.last_update = response.data.request.status_update.when;
                } else {
                    userStatus = response.data.request.user.last_status;
                    $scope.last_update = response.data.request.user.when;
                }

                if (userStatus == "sick"){
                    $scope.status = "Sakit";
                    $scope.status_img = "img/img-sakit.png";
                } else if (userStatus == "suspect"){
                    $scope.status = "Suspect Covid-19";
                    $scope.status_img = "img/img-corona.png";
                } else {
                    $scope.status = "Sehat";
                    $scope.status_img = "img/img-sehat.png";
                }
            } else {
                var alertPopup = $ionicPopup.alert({
                    title: "Status User Tidak Ditemukan",
                    template: response.data.request.message
                });
            }
        }, function myError(response) {
            myHelper.showToast("Koneksi gagal, coba kembali nanti");
            $scope.btnNext = false;
        });
    }

    $scope.$on('$ionicView.afterEnter', function() {
        if (localStorage.getItem("userId")){
            $scope.loadData();
        } else {
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go("app.registrasi");
        }
    })
});